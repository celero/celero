<?php
$page = "portfolio";
include("../header.php");
?>
<div itemscope itemtype="http://www.schema.org/WebPage">
    <section id="feature">
        <div class="container">
            <div class="feature-content">
                <h2>State of the art</h2>

                <p>
                    Please take the time to have a look at some of our recent work. Solutions that sing and execution to
                    match, our team produces winning results that are as unique as you are.
                </p>

                <p class="last">
                    Like what you see? Kickoff with with our
                    <a title="Celero Project Brief" itemprop="breadcrumb" href="../brief/">project brief</a>.
                </p>
            </div>
        </div>
    </section>

    <section id="portfolio" itemprop="mainContentOfPage">
        <div class="container">
            <div class="section-subtitle">
                <h2>Projects we've recently worked on</h2>
            </div>

            <div class="module">
                <div class="image-gallery" itemscope itemtype="http://schema.org/ImageGallery">

                    <figure class="gallery" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_domain_1.jpg" itemprop="contentUrl" data-size="1060x2088">
                            <img src="/images/projects/project_domain.png" itemprop="thumbnail" alt="Domain Group" width="396" height="297" />

                            <span class="rollover"></span>
                        </a>

                        <figcaption class="hidden" itemprop="caption description">
                            Careers page with currently available career opportunities
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_domain_2.png" itemprop="contentUrl" data-size="1060x1807"></a>

                        <figcaption class="hidden" itemprop="caption description">
                            Career Detail page with career information and job application
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_domain_3.png" itemprop="contentUrl" data-size="1060x1007"></a>

                        <figcaption itemprop="caption description">
                            Ad Specs page with available ad platforms
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_domain_4.png" itemprop="contentUrl" data-size="1060x1030"></a>

                        <figcaption itemprop="caption description">
                            Ad Platform page with available ad products
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_domain_5.png" itemprop="contentUrl" data-size="1060x2680"></a>

                        <figcaption itemprop="caption description">
                            Ad Product Detail page with product information and specifications
                        </figcaption>
                    </figure>
                </div>

                <h5>
                    Domain Group

                    <span class="note">-
                        <a href="http://www.domain.com.au/group/" target="_blank">
                            View Site
                        </a>
                    </span>
                </h5>

                <p>Property marketing solutions and tools</p>

                <a class="role first" href="../services/#development">Website Development</a>
            </div>

            <div class="module">
                <div class="image-gallery" itemscope itemtype="http://schema.org/ImageGallery">

                    <figure class="gallery" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_leapfrog_1.png" itemprop="contentUrl" data-size="1060x567">
                            <img src="/images/projects/project_leapfrog.png" itemprop="thumbnail" alt="Team Exile5" width="396" height="297" />

                            <span class="rollover"></span>
                        </a>

                        <figcaption class="hidden" itemprop="caption description">
                            Home page
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_leapfrog_2.png" itemprop="contentUrl" data-size="1060x567"></a>

                        <figcaption class="hidden" itemprop="caption description">
                            What page with content slider
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_leapfrog_3.png" itemprop="contentUrl" data-size="1060x567"></a>

                        <figcaption itemprop="caption description">
                            Connect page
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_leapfrog_4.png" itemprop="contentUrl" data-size="1060x567"></a>

                        <figcaption itemprop="caption description">
                            Testimonial page
                        </figcaption>
                    </figure>
                </div>

                <h5>
                    Leapfrog Executive Search

                    <span class="note">-
                        <a href="http://www.leapfrogsearch.com" target="_blank">
                            View Site
                        </a>
                    </span>
                </h5>

                <p>Executive search consultancy</p>

                <a class="role first" href="../services/#development">Website Development</a>
            </div>

            <div class="module">
                <div class="image-gallery" itemscope itemtype="http://schema.org/ImageGallery">

                    <figure class="gallery" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_x5_1.png" itemprop="contentUrl" data-size="1060x1432">
                            <img src="/images/projects/project_x5.png" itemprop="thumbnail" alt="Team Exile5" width="396" height="297" />

                            <span class="rollover"></span>
                        </a>

                        <figcaption class="hidden" itemprop="caption description">
                            Home page with news slider and current and upcoming events
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_x5_2.png" itemprop="contentUrl" data-size="1060x1842"></a>

                        <figcaption class="hidden" itemprop="caption description">
                            Article page with comments and social sharing
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_x5_3.png" itemprop="contentUrl" data-size="1060x1434"></a>

                        <figcaption itemprop="caption description">
                            Event Detail page with Twitch stream and chat integration and social sharing
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_x5_4.png" itemprop="contentUrl" data-size="1060x1263"></a>

                        <figcaption itemprop="caption description">
                            Division page with player profiles and division articles and past events
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_x5_5.png" itemprop="contentUrl" data-size="1060x1307"></a>

                        <figcaption itemprop="caption description">
                            Player Profile page with player info, gear, and social sharing
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_x5_6.png" itemprop="contentUrl" data-size="1060x1196"></a>

                        <figcaption itemprop="caption description">
                            Media page with Facebook API integration to display latest albums and photos from Facebook and
                            YouTube API integration to display latest videos from YouTube
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_x5_7.png" itemprop="contentUrl" data-size="1060x1196"></a>

                        <figcaption itemprop="caption description">
                            YouTube Video page with embedded video and info from YouTube API
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_x5_8.png" itemprop="contentUrl" data-size="1060x567"></a>

                        <figcaption itemprop="caption description">
                            Facebook Album page with images from Facebook album displayed as a gallery
                        </figcaption>
                    </figure>
                </div>

                <h5>
                    Team Exile5

                    <span class="note">-
                        <a href="http://www.team-exile5.org" target="_blank">
                            View Site
                        </a>
                    </span>
                </h5>

                <p>Professional eSports organisation</p>

                <a class="role first" href="../services/#development">Website Development</a>
                <a class="role" href="../services/#seo">SEO</a>
            </div>

            <div class="module">
                <div class="image-gallery" itemscope itemtype="http://schema.org/ImageGallery">

                    <figure class="gallery" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_creativeco_1.jpg" itemprop="contentUrl" data-size="1060x4070">
                            <img src="/images/projects/project_creativeco.png" itemprop="thumbnail" alt="The Creative Co" width="396" height="297" />

                            <span class="rollover"></span>
                        </a>

                        <figcaption class="hidden" itemprop="caption description">
                            Home page
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_creativeco_2.png" itemprop="contentUrl" data-size="1060x1230"></a>

                        <figcaption class="hidden" itemprop="caption description">
                            Project Detail page with image slider and project navigation
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_creativeco_3.jpg" itemprop="contentUrl" data-size="1060x1349"></a>

                        <figcaption itemprop="caption description">
                            Staff Profile page with image gallery and staff navigation
                        </figcaption>
                    </figure>
                </div>

                <h5>
                    The Creative Co

                    <span class="note">-
                        <a href="http://www.thecreativeco.com.au" target="_blank">
                            View Site
                        </a>
                    </span>
                </h5>

                <p>Creative agency and design collective</p>

                <a class="role first" href="../services/#development">Website Development</a>
                <a class="role" href="../services/#redesign">Website Redesign</a>
            </div>

            <div class="module">
                <div class="image-gallery" itemscope itemtype="http://schema.org/ImageGallery">

                    <figure class="gallery" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_stash_house_1.jpg" itemprop="contentUrl" data-size="1060x2391">
                            <img src="/images/projects/project_stash_house.png" itemprop="thumbnail" alt="The Stash House" width="396" height="297" />

                            <span class="rollover"></span>
                        </a>

                        <figcaption class="hidden" itemprop="caption description">
                            Home page with banner slider and integrated Instagram feed
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_stash_house_2.jpg" itemprop="contentUrl" data-size="1060x2239"></a>

                        <figcaption class="hidden" itemprop="caption description">
                            Product Category page with category and sub-category navigation
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_stash_house_3.png" itemprop="contentUrl" data-size="1060x1592"></a>

                        <figcaption itemprop="caption description">
                            Product Detail page with image gallery and related items
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_stash_house_4.png" itemprop="contentUrl" data-size="1060x1066"></a>

                        <figcaption itemprop="caption description">
                            Shopping Cart page with discounts, gift cards, and shipping estimates
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_stash_house_5.jpg" itemprop="contentUrl" data-size="1060x3536"></a>

                        <figcaption itemprop="caption description">
                            About page with store locations
                        </figcaption>
                    </figure>
                </div>

                <h5>
                    The Stash House

                    <span class="note">-
                        <a href="http://thestashhouse.com.au" target="_blank">
                            View Site
                        </a>
                    </span>
                </h5>

                <p>Street and skate clothing and accessories store</p>

                <a class="role first" href="../services/#development">Website Development</a>
            </div>

            <div class="module">
                <div class="image-gallery" itemscope itemtype="http://schema.org/ImageGallery">

                    <figure class="gallery" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_doyles_1.png" itemprop="contentUrl" data-size="1060x2055">
                            <img src="/images/projects/project_doyles.png" itemprop="thumbnail" alt="Doyles Incar" width="396" height="297" />

                            <span class="rollover"></span>
                        </a>

                        <figcaption class="hidden" itemprop="caption description">
                            Home page
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_doyles_2.png" itemprop="contentUrl" data-size="1060x1675"></a>

                        <figcaption class="hidden" itemprop="caption description">
                            Colour Range page
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_doyles_3.png" itemprop="contentUrl" data-size="1060x1379"></a>

                        <figcaption itemprop="caption description">
                            Product Category page
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_doyles_4.png" itemprop="contentUrl" data-size="1060x1950"></a>

                        <figcaption itemprop="caption description">
                            Product Detail page with image gallery, product description and related products
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_doyles_5.png" itemprop="contentUrl" data-size="1060x1952"></a>

                        <figcaption itemprop="caption description">
                            Contact page with interactive store locator and contact form
                        </figcaption>
                    </figure>
                </div>

                <h5>
                    Doyles Incar

                    <span class="note">-
                        <a href="http://www.doylesincar.com.au" target="_blank">
                            View Site
                        </a>
                    </span>
                </h5>

                <p>Automotive upgrade specialists</p>

                <a class="role first" href="../services/#development">Website Development</a>
            </div>

            <div class="module bottom3">
                <div class="image-gallery" itemscope itemtype="http://schema.org/ImageGallery">

                    <figure class="gallery" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_veronika_maine_1.jpg" itemprop="contentUrl" data-size="1060x6025">
                            <img src="/images/projects/project_veronika_maine.png" itemprop="thumbnail" alt="Veronika Maine" width="396" height="297" />

                            <span class="rollover"></span>
                        </a>

                        <figcaption class="hidden" itemprop="caption description">
                            EDM Campaign email template
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_veronika_maine_2.png" itemprop="contentUrl" data-size="1060x1810"></a>

                        <figcaption class="hidden" itemprop="caption description">
                            Exclusive Member Offer email template
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_veronika_maine_3.png" itemprop="contentUrl" data-size="1060x1376"></a>

                        <figcaption class="hidden" itemprop="caption description">
                            Birthday Reward email template
                        </figcaption>
                    </figure>
                </div>

                <h5>
                    Veronika Maine
                </h5>

                <p>Australian fashion brand</p>

                <a class="role first" href="../services/#marketing">Marketing</a>
            </div>

            <div class="module bottom3">
                <div class="image-gallery" itemscope itemtype="http://schema.org/ImageGallery">

                    <figure class="gallery" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_robins_kitchen_1.png" itemprop="contentUrl" data-size="1060x2197">
                            <img src="/images/projects/project_robins_kitchen.png" itemprop="thumbnail" alt="Robins Kitchen" width="396" height="297" />

                            <span class="rollover"></span>
                        </a>

                        <figcaption class="hidden" itemprop="caption description">
                            Home page
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_robins_kitchen_2.png" itemprop="contentUrl" data-size="1060x1934"></a>

                        <figcaption class="hidden" itemprop="caption description">
                            Product Detail page
                        </figcaption>
                    </figure>
                </div>

                <h5>
                    Robins Kitchen

                    <span class="note">-
                        <a href="http://stores.ebay.com.au/Robins-Kitchen-Official" target="_blank">
                            View Site
                        </a>
                    </span>
                </h5>

                <p>Kitchenware and homewares store</p>

                <a class="role first" href="../services/#development">Website Development</a>
            </div>

            <div class="module bottom3 bottom2">
                <div class="image-gallery" itemscope itemtype="http://schema.org/ImageGallery">

                    <figure class="gallery" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_marcelo_1.png" itemprop="contentUrl" data-size="1060x1052">
                            <img src="/images/projects/project_marcelo.png" itemprop="thumbnail" alt="Dr Marcelo Nascimento" width="396" height="297" />

                            <span class="rollover"></span>
                        </a>

                        <figcaption class="hidden" itemprop="caption description">
                            Home page
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_marcelo_2.png" itemprop="contentUrl" data-size="1060x1067"></a>

                        <figcaption class="hidden" itemprop="caption description">
                            Patient Information page
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_marcelo_3.png" itemprop="contentUrl" data-size="1060x853"></a>

                        <figcaption class="hidden" itemprop="caption description">
                            Contact page with contact form and embedded map showing the location of the practice
                        </figcaption>
                    </figure>

                    <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="/images/projects/project_marcelo_4.png" itemprop="contentUrl" data-size="1060x602"></a>

                        <figcaption class="hidden" itemprop="caption description">
                            Glossary page with definitions for relevant medical terms
                        </figcaption>
                    </figure>
                </div>

                <h5>
                    <h5>Dr Marcelo Nascimento

                    <span class="note">-
                        <a href="http://www.drnascimento.com.au" target="_blank">
                            View Site
                        </a>
                    </span>
                </h5>

                <p>Gold Coast medical practice</p>

                <a class="role first" href="../services/#redesign">Website Redesign</a>
                <a class="role" href="../services/#seo">SEO</a>
                <a class="role" href="../services/#marketing">Marketing</a>
            </div>
        </div>
    </section>
</div>

<?php include("../footer.php"); ?>
<?php include("../photoswipe.php"); ?>

</body>
</html>
