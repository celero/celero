<section id="footer-brief" class="<?php echo ($page != 'home' ? 'light' : ''); echo ($page == 'brief' ? ' brief' : ''); ?>">
    <div class="container">
        <div class="section-subtitle">
            <h2>Want to get started?</h2>
        </div>
        <div class="get-started">
            <p>If you already have an idea, then what are you waiting for?<br>
                Get started right away with our <a title="Celero Project Brief" class="-brief_link" href="<?php echo $root ?>brief/">project brief</a>!</p>
        </div>
        <div class="image">
            <a class="replace" href="<?php echo $root ?>brief/"><span></span></a>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="contact-details"><a href="<?php echo $root ?>contact/">Contact Us</a> or email <a href="mailto:contact@celero.com.au">contact@celero.com.au</a></div>
        <div class="copyright">Copyright &copy; <?php echo date('Y'); ?> Celero Website Development. &nbsp; ABN: 25962670752</div>
    </div>
</footer>
</div>

<script src="<?php echo $root ?>javascripts/jquery.bxslider.min.js"></script>
<script src="<?php echo $root ?>javascripts/jquery.smooth-scroll.min.js"></script>
<script src="<?php echo $root ?>javascripts/jquery.validate.min.js"></script>
<script src="<?php echo $root ?>javascripts/photoswipe.min.js"></script>
<script src="<?php echo $root ?>javascripts/photoswipe-ui-default.min.js"></script>
<script src="<?php echo $root ?>javascripts/javascripts.js?v=2"></script>

<?php
$servicesjs = '<script src="../javascripts/services.js"></script>';

if ($page == 'services') {
    echo $servicesjs;
}
?>