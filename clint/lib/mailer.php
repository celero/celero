<?php
	require_once 'swift/swift_required.php';

	$name    = $_POST['name'];
	$email   = $_POST['email'];
	$number  = $_POST['number'];
	$message = $_POST['message'];

	// Phone number is optional, therefore if not provided, the e-mail received will reflect that
	if ($number == "") {
	    $number = "Not provided";
	}
	$msg = "Name: $name\n";
	$msg .= "Email: $email\n";
	$msg .= "Number: $number\n";
	$msg .= "$message \n";

	$transport = Swift_SmtpTransport::newInstance('smtp.celero.com.au', 25)
		->setUsername('contact@celero.com.au')
		->setPassword('@Celero1');

	$mailer = Swift_Mailer::newInstance($transport);

	$message = Swift_Message::NewInstance('Resume Response')
		->setFrom(array($email => $name))
		->setTo(array('clint@celero.com.au' => 'Clint'))
		->setBody($msg);

	$result = $mailer->send($message);

	if ($result) {
	    echo "Success";
	}
	else {
	    echo "Error";
	}
?>

