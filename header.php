<?php $root = "/";?>

<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en" itemscope itemtype="http://www.schema.org/Organization"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Celero - Website Development, Website Redesign and SEO Company in Newcastle Region</title>
    <meta name="description" content="Celero - Website Development, Website Redesign and SEO Company in Newcastle Region">
    <meta name="author" content="Celero Website Development">
    <meta itemprop="url" name="url" content="http://www.celero.com.au">
    <meta itemprop="email" name="email" content="contact@celero.com.au">
    <meta itemprop="logo" property="og:image" content="http://www.celero.com.au/images/logo-square.png">
    <meta property="og:title" content="Celero Website Development">
    <meta property="og:url" content="http://www.celero.com.au">
    <meta itemprop="name" property="og:site_name" content="Celero Website Development">
    <meta itemprop="reviews" property="reviews" content="Celero has been fantastic at providing professional advice and expertise for our business.">
    <meta itemprop="reviews" property="reviews" content="10/10">
    <meta itemprop="reviews" property="reviews" content="The guys offered amazing support and delivered an amazing product, really fast.">

    <meta property="og:type" content="Website">
    <meta property="og:description" content="Celero - Website Development, Website Redesign and SEO Company in Newcastle Region"">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="<?php echo $root ?>stylesheets/styles.css">
    <link rel="stylesheet" href="<?php echo $root ?>stylesheets/photoswipe.css">
    <link rel="stylesheet" href="<?php echo $root ?>images/photoswipe/default-skin/default-skin.css">

    <!-- JavaScript -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="<?php echo $root ?>javascripts/css-browser-selector-min.js"></script>

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons -->
    <link rel="icon" type="image/png" href="http://www.celero.com.au/favicon.png">
    <!--[if IE]>
    <link rel="SHORTCUT ICON" href="http://www.celero.com.au/favicon.ico"/><![endif]-->
    <link rel="apple-touch-icon" href="<?php echo $root ?>apple-touch-icon-iphone.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $root ?>apple-touch-icon-ipad.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $root ?>apple-touch-icon-iphone-retina.png"/>
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $root ?>apple-touch-icon-ipad-retina.png"/>

</head>
<body>
<div id="content-container">

    <header>
        <div class="container">
            <div id="logo">
                <h1><a href="<?php echo $root ?>">Celero Web Development</a></h1>
            </div>
            <div class="definition">verb: to accelerate, to do quickly</div>
            <nav id="primary">
                <ul class="nav">
                    <li>
                        <a href="<?php echo $root ?>" class="first">Home</a>
                    <li class="<?php echo ($page == 'services' ? 'active' : '')?>">
                        <a href="<?php echo $root ?>services/">Services</a>
                    </li>
                    <li class="<?php echo ($page == 'portfolio' ? 'active' : '')?>">
                        <a href="<?php echo $root ?>portfolio/">Portfolio</a>
                    </li>
                    <li class="<?php echo ($page == 'about' ? 'active' : '')?>">
                        <a href="<?php echo $root ?>about/">About Us</a>
                    </li>
                    <li class="<?php echo ($page == 'contact' ? 'active' : '')?>">
                        <a href="<?php echo $root ?>contact/">Contact</a>
                    </li>
                </ul>
                <a class="brief<?php echo ($page == 'brief' ? ' active' : '')?>"
                   href="<?php echo $root ?>brief/">Get a Quote</a>
            </nav>
            <nav id="select"></nav>
        </div>
    </header>
