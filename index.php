<?php
$page = "home";
include("header.php");
?>

<div itemscope itemtype="http://www.schema.org/WebPage">
<section id="showcase" xmlns="http://www.w3.org/1999/html">
    <div class="container">
        <div class="statement">

            <ul class="bxslider">
                <li>
                    <h2 itemprop="name">Celero</h2>

                    <p itemprop="description">Celero is a website development company based in Newcastle. We specialise
                        in
                        creating customer-focused, user friendly websites. Celero make creating an effective website and
                        going online an easy and stress-free process for you!</p>
                </li>
                <li itemprop="text">
                    <h2>State of the Art</h2>

                    <p>Our websites utilise the latest in web development technologies to provide cutting edge, scalable
                        solutions tailored to our client's needs.</p>
                </li>
                <li itemprop="text">
                    <h2>Fast</h2>

                    <p>Utilising the latest industry standard frameworks, Celero provide a fast, flexible approach for
                        designing and implementing products, with active client involvement.</p>
                </li>
                <li itemprop="text">
                    <h2>Affordable</h2>

                    <p>With a small and highly skilled development team, Celero provide state of the art websites at an
                        affordable price.</p>
                </li>
            </ul>

            <a href="#services-module" class="button smoothScroll">How can we help your business?</a>
        </div>

        <div class="services-module" id="services-module">
            <a class="learn-more development" href="services/#development"></a>
            <h4 itemprop="name">Website Development</h4>

            <div class="description">
                <p itemprop="description">We provide complete website design and implementation using state
                    of the art technologies to create modern, easy to use websites.</p>
            </div>

            <a class="learn" itemprop="breadcrumb" href="services/#development">Learn More</a>
        </div>
        <div class="services-module">
            <a class="learn-more redesign" href="services/#redesign"></a>
            <h4 itemprop="name">Website Redesign</h4>

            <div class="description">
                <p itemprop="description">Have an existing website, but want to improve its performance or
                    design? Celero provide solutions for improving existing websites.</p>
            </div>

            <a class="learn" itemprop="breadcrumb" href="services/#redesign">Learn More</a>
        </div>
        <div class="services-module">
            <a class="learn-more seo" href="services/#seo"></a>
            <h4 itemprop="name">Search Engine Optimisation</h4>

            <div class="description">
                <p itemprop="description">Increase traffic to your website by utilising the power of search
                    engine optimisations to improve your website's online presence.</p>
            </div>
            <a class="learn" href="services/#seo">Learn More</a>
        </div>
        <div class="services-module">
            <a class="learn-more marketing" href="services/#marketing"></a>
            <h4 itemprop="name">Marketing</h4>

            <div class="description">
                <p itemprop="description">Celero is a one stop shop for all for your marketing needs. We offer a
                    customised service to enhance your website marketing opportunities.</p>
            </div>
            <a class="learn" itemprop="breadcrumb" href="services/#marketing">Learn More</a>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        //BOXSLIDER SETUP
        $('.bxslider').bxSlider({pager: false});

        $('.bxslider').css('opacity', 1);
    });
</script>

<section id="home-portfolio">
    <div class="container">
        <div class="section-subtitle">
            <h2>Some projects we've recently worked on</h2>
        </div>

        <div class="module">
            <div class="image-gallery" itemscope itemtype="http://schema.org/ImageGallery">

                <figure class="gallery" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_domain_1.jpg" itemprop="contentUrl" data-size="1060x2088">
                        <img src="/images/projects/project_domain.png" itemprop="thumbnail" alt="Domain Group" width="396" height="297" />

                        <span class="rollover"></span>
                    </a>

                    <figcaption class="hidden" itemprop="caption description">
                        Careers page with currently available career opportunities
                    </figcaption>
                </figure>

                <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_domain_2.png" itemprop="contentUrl" data-size="1060x1807"></a>

                    <figcaption class="hidden" itemprop="caption description">
                        Career Detail page with career information and job application
                    </figcaption>
                </figure>

                <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_domain_3.png" itemprop="contentUrl" data-size="1060x1007"></a>

                    <figcaption itemprop="caption description">
                        Ad Specs page with available ad platforms
                    </figcaption>
                </figure>

                <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_domain_4.png" itemprop="contentUrl" data-size="1060x1030"></a>

                    <figcaption itemprop="caption description">
                        Ad Platform page with available ad products
                    </figcaption>
                </figure>

                <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_domain_5.png" itemprop="contentUrl" data-size="1060x2680"></a>

                    <figcaption itemprop="caption description">
                        Ad Product Detail page with product information and specifications
                    </figcaption>
                </figure>
            </div>

            <h5>
                Domain Group

                    <span class="note">-
                        <a href="http://www.domain.com.au/group/" target="_blank">
                            View Site
                        </a>
                    </span>
            </h5>

            <p>Property marketing solutions and tools</p>
        </div>

        <div class="module">
            <div class="image-gallery" itemscope itemtype="http://schema.org/ImageGallery">

                <figure class="gallery" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_leapfrog_1.png" itemprop="contentUrl" data-size="1060x567">
                        <img src="/images/projects/project_leapfrog.png" itemprop="thumbnail" alt="Team Exile5" width="396" height="297" />

                        <span class="rollover"></span>
                    </a>

                    <figcaption class="hidden" itemprop="caption description">
                        Home page
                    </figcaption>
                </figure>

                <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_leapfrog_2.png" itemprop="contentUrl" data-size="1060x567"></a>

                    <figcaption class="hidden" itemprop="caption description">
                        What page with content slider
                    </figcaption>
                </figure>

                <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_leapfrog_3.png" itemprop="contentUrl" data-size="1060x567"></a>

                    <figcaption itemprop="caption description">
                        Connect page
                    </figcaption>
                </figure>

                <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_leapfrog_4.png" itemprop="contentUrl" data-size="1060x567"></a>

                    <figcaption itemprop="caption description">
                        Testimonial page
                    </figcaption>
                </figure>
            </div>

            <h5>
                Leapfrog Executive Search

                    <span class="note">-
                        <a href="http://www.leapfrogsearch.com" target="_blank">
                            View Site
                        </a>
                    </span>
            </h5>

            <p>Executive search consultancy</p>
        </div>

        <div class="module">
            <div class="image-gallery" itemscope itemtype="http://schema.org/ImageGallery">

                <figure class="gallery" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_x5_1.png" itemprop="contentUrl" data-size="1060x1432">
                        <img src="/images/projects/project_x5.png" itemprop="thumbnail" alt="Team Exile5" width="396" height="297" />

                        <span class="rollover"></span>
                    </a>

                    <figcaption class="hidden" itemprop="caption description">
                        Home page with news slider and current and upcoming events
                    </figcaption>
                </figure>

                <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_x5_2.png" itemprop="contentUrl" data-size="1060x1842"></a>

                    <figcaption class="hidden" itemprop="caption description">
                        Article page with comments and social sharing
                    </figcaption>
                </figure>

                <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_x5_3.png" itemprop="contentUrl" data-size="1060x1434"></a>

                    <figcaption itemprop="caption description">
                        Event Detail page with Twitch stream and chat integration and social sharing
                    </figcaption>
                </figure>

                <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_x5_4.png" itemprop="contentUrl" data-size="1060x1263"></a>

                    <figcaption itemprop="caption description">
                        Division page with player profiles and division articles and past events
                    </figcaption>
                </figure>

                <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_x5_5.png" itemprop="contentUrl" data-size="1060x1307"></a>

                    <figcaption itemprop="caption description">
                        Player Profile page with player info, gear, and social sharing
                    </figcaption>
                </figure>

                <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_x5_6.png" itemprop="contentUrl" data-size="1060x1196"></a>

                    <figcaption itemprop="caption description">
                        Media page with Facebook API integration to display latest albums and photos from Facebook and
                        YouTube API integration to display latest videos from YouTube
                    </figcaption>
                </figure>

                <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_x5_7.png" itemprop="contentUrl" data-size="1060x1196"></a>

                    <figcaption itemprop="caption description">
                        YouTube Video page with embedded video and info from YouTube API
                    </figcaption>
                </figure>

                <figure class="gallery hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="/images/projects/project_x5_8.png" itemprop="contentUrl" data-size="1060x567"></a>

                    <figcaption itemprop="caption description">
                        Facebook Album page with images from Facebook album displayed as a gallery
                    </figcaption>
                </figure>
            </div>

            <h5>
                Team Exile5

                    <span class="note">-
                        <a href="http://www.team-exile5.org" target="_blank">
                            View Site
                        </a>
                    </span>
            </h5>

            <p>Professional eSports organisation</p>
        </div>
</section>

<section id="testimonials">
    <div class="container">
        <div class="section-subtitle">
            <h2>Here's what our clients say about working with Celero</h2>
        </div>
        <div class="box">
            <p id="first" class="shown" itemprop="review">I have been absolutely impressed with the team at Celero. They
                really excelled in two important areas.<br><br>
                First, they took the time to truly understand how I wanted to
                deliver my message on the web. Second, they understood that my patients are not all typical web users
                and built the site’s design and functionality around that. I could not ask for more. I am extremely
                happy with Celero.
                <span itemprop="reviewedBy"><br><br>Dr Marcelo Nascimento, Private Practice</span>
            </p>

            <p id="second" itemprop="review">Celero took considerable time to find out exactly what my
                business was about and
                who my potential customers were.<br><br>
                During the construction phase, they regularly consulted and kept me up to date with progress. They
                included innovative features and integrated the content/images to best effect.
                <span itemprop="reviewedBy"><br><br>Peter Vaughan, Out and About Adventures</span>
            </p>

            <p id="third" itemprop="review">Celero has been fantastic at providing professional advice and expertise
                for our business.
                Their friendly, positive attitude and responsiveness made the consultation process very easy and hassle
                free.<br><br>
                I enjoyed working with Celero and will continue to use and recommend them in the future.
                <span itemprop="reviewedBy"><br><br>Bevan Ramsden, Ramsden Training</span>
            </p>
        </div>
        <ul>
            <li class="active">
                <div class="person" data-quote="#first">
                    <a class="quote"><img src="images/marcelo.png"
                                          alt="Dr Marcelo Nascimento, Private Practice" width="216"
                                          height="216"></a>

                    <p class="name">Dr Marcelo Nascimento</p>

                    <p class="title">Owner & Doctor</p>

                    <p class="company">Private Medical Practice</p>
                </div>
            </li>
            <li>
                <div class="person" data-quote="#second">
                    <img src="images/peter.png" alt="Peter Vaughan, Out and About Adventures" width="216" height="216">


                    <p class="name">Peter Vaughan</p>

                    <p class="title">Owner & Operator</p>

                    <p class="company">Out and About Adventures</p>
                </div>
            </li>
            <li>
                <div class="person" data-quote="#third">
                    <a class="quote"><img src="images/bevan.png"
                                          alt="Bevan Ramsden, Ramsden Training" width="216"
                                          height="216"></a>

                    <p class="name">Bevan Ramsden</p>

                    <p class="title">Technical Director</p>

                    <p class="company">Ramsden Training</p>
                </div>
            </li>
        </ul>
    </div>
</section>
</div>

<?php include("footer.php"); ?>
<?php include("photoswipe.php"); ?>

</body>
</html>
