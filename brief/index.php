<?php
$page = "brief";
include("../header.php");
?>

<section id="feature" xmlns="http://www.w3.org/1999/html">
    <div class="container">
        <div class="feature-content">
            <h2>We're all ears</h2>

            <p>Eager start-ups, established corporations and everything in between, no matter who you are or how big
                your project is we'd love to discuss how we can help.</p>

            <p class="last">We can't wait to hear from you.</p>
        </div>
    </div>
</section>

<section id="brief">
<div class="container" itemscope itemtype="http://www.schema.org/WebPage">
<div class="section-subtitle">
    <h2>We know no one likes filling out forms, but…</h2>
</div>
<div class="section-description">
    <p>Our project brief will only take five minutes to complete. It's a great starting point for us in terms of
        beginning to understand your requirements, budget and schedule.</p>

    <p class="last">Alternatively, if you'd like to just contact us directly you can send us an email at
        <a href="mailto:contact@celero.com.au">contact@celero.com.au</a> or visit our <a href="../contact/">Contact</a>
        page and we'll get back to you shortly.</p>
</div>

<form id="brief-form" action="../lib/brief.php" method="POST">
<div class="divider">
    <h3>Your Contact Information</h3>

    <p>* required field</p>

    <div class="line"></div>
</div>

<div class="section">
    <div class="input">
        <label for="name">Your name *</label>
        <input id="name" type="text" name="name" class="required" minlength="2">
    </div>
    <div class="helper">
        <div class="helper-content">
            <p>This one is easy, we hope!</p>
        </div>
    </div>
</div>

<div class="section">
    <div class="input">
        <label for="email">Email *</label>
        <input id="email" type="email" name="email" novalidate="novalidate" class="required email">
    </div>
    <div class="helper">
        <div class="helper-content">
            <p>One that you check regularly so we can get in touch.</p>
        </div>
    </div>
</div>

<div class="section">
    <div class="input">
        <label for="number">Telephone</label>
        <input id="number" type="tel" name="number">
    </div>
    <div class="helper">
        <div class="helper-content">
            <p>The number you're most likely to pick up.</p>
        </div>
    </div>
</div>

<div class="section">
    <div class="input">
        <label for="business">Business/Organisation</label>
        <input id="business" type="text" name="business">
    </div>
    <div class="helper">
        <div class="helper-content">
            <p>Who you work for or represent.</p>
        </div>
    </div>
</div>

<div class="section">
    <div class="input">
        <label for="url">Current website address (if you have one)</label>
        <input id="url" type="text" name="url" value="http://">
    </div>
    <div class="helper">
        <div class="helper-content">
            <p>So we can see what you're working with.</p>
        </div>
    </div>
</div>

<div class="divider">
    <h3>About Your Project</h3>

    <div class="line"></div>
</div>

<div class="section">
    <div class="input">
        <p>
            <label>What kind of project is this? *</label><br>
            <input id="type_1" type="checkbox" name="project_type[]" value="Website Development">
            <label for="type_1">Website Development</label><br>
            <input id="type_2" type="checkbox" name="project_type[]" value="Website Redesign">
            <label for="type_2">Website Redesign</label><br>
            <input id="type_3" type="checkbox" name="project_type[]" value="Search Engine Optimisation">
            <label for="type_3">Search Engine Optimisation</label><br>
            <input id="type_4" type="checkbox" name="project_type[]" value="Marketing">
            <label for="type_4">Marketing</label>
        </p>
    </div>
    <div class="helper">
        <div class="helper-content">
            <p>This helps us to decide which questions we need to ask you next.</p>
        </div>
    </div>
</div>

<div class="section">
    <div class="input">
        <label for="description">Briefly describe your project *</label>
        <textarea id="description" name="description" class="required"></textarea>
    </div>
    <div class="helper">
        <div class="helper-content">
            <p>An overall outline. You might like to mention:</p>
            <ul>
                <li>What are your objectives?</li>
                <li>Who is your target audience?</li>
                <li>Who are your competitors?</li>
                <li>Your functional requirements.</li>
            </ul>
        </div>
    </div>
</div>

<div class="section">
    <div class="input">
        <p>
            <label>What is your budget? *</label><br>
            <input id="budget_1" type="radio" value="Less than $1,500" name="budget" class="required">
            <label for="budget_1">< $1,500</label><br>
            <input id="budget_2" type="radio" value="$1,500 - $3,000" name="budget">
            <label for="budget_2">$1,500 - $3,000</label><br>
            <input id="budget_3" type="radio" value="$3,000 - $6,000" name="budget">
            <label for="budget_3">$3,000 - $6,000</label><br>
            <input id="budget_4" type="radio" value="$6,000 - $10,000" name="budget">
            <label for="budget_4">$6,000 - $10,000</label><br>
            <input id="budget_5" type="radio" value="More than $10,000" name="budget">
            <label for="budget_5">$10,000 +</label><br>
            <input id="budget_6" type="radio" value="Unsure" name="budget">
            <label for="budget_6">Unsure</label>
        </p>
    </div>
    <div class="helper">
        <div class="helper-content">
            <p>This gives us an idea of the scope of your project and without it, it is very difficult to
                gauge the time we can dedicate to you.</p>
        </div>
    </div>
</div>

<div class="section">
    <div class="input">
        <p>
            <label>Do you have a deadline? *</label><br>
            <input id="deadline_1" type="radio" value="Less than 1 month" name="deadline" class="required">
            <label for="deadline_1">Less than 1 month</label><br>
            <input id="deadline_2" type="radio" value="1 - 3 months" name="deadline">
            <label for="deadline_2">1 - 3 months</label><br>
            <input id="deadline_3" type="radio" value="More than 3 months" name="deadline">
            <label for="deadline_3">More than 3 months</label><br>
            <input id="deadline_none" type="radio" value="Time isn't an issue" name="deadline">
            <label for="deadline_none">Time isn't an issue</label>
        </p>
    </div>
    <div class="helper">
        <div class="helper-content">
            <p>Unfortunately yesterday isn't possible...</p>
        </div>
    </div>
</div>

<div class="section">
    <div class="input">
        <p>
            <label>What would you like the next step to be moving forward with us? *</label><br>
            <input id="next_step_1" type="radio" value="Meet for initial consultation" name="next_step"
                   class="required">
            <label for="next_step_1">Meet for initial consultation</label><br>
            <input id="next_step_2" type="radio" value="Speak on the phone" name="next_step">
            <label for="next_step_2">Speak on the phone</label><br>
            <input id="next_step_3" type="radio" value="Communicate over email" name="next_step">
            <label for="next_step_3">Communicate over email</label><br>
            <input id="next_step_4" type="radio" value="Request a proposal" name="next_step">
            <label for="next_step_4">Request a proposal</label>
        </p>
    </div>
    <div class="helper">
        <div class="helper-content">
            <p>What you want the next step to be after we've received and reviewed your project brief.</p>
        </div>
    </div>
</div>

<div class="divider">
    <h3>Additional Information</h3>

    <div class="line"></div>
</div>

<div class="section">
    <div class="input">
        <label for="hear">How did you hear about us?</label>
        <input id="hear" type="text" name="hear">
    </div>
    <div class="helper">
        <div class="helper-content">
            <p>How you heard about Celero or found our website.</p>
        </div>
    </div>
</div>

<div class="section">
    <div class="input">
        <label for="additional_info">Additional information or questions</label>
        <textarea id="additional_info" name="additional_info"></textarea>
    </div>
    <div class="helper">
        <div class="helper-content">
            <p>Any additional information you want to tell us or questions you want to ask.</p>
        </div>
    </div>
</div>

<div class="divider">
    <div class="line no-heading"></div>
</div>

<div class="submit-section">
    <div class="section-description">
        <p>All that leaves to do is to click send and we'll get back to you as soon as we can!</p>
    </div>

    <div class="submit">
        <input type="submit" value="Send Project Brief">
    </div>
</div>
</form>

<div id="message-success" class="message-response">
    <p>Your project brief has been submitted! Thanks for taking the time to fill it out, we'll get back to you
        shortly.</p>
</div>
<div id="message-error" class="message-response">
    <p>Sorry, something went wrong. Please fill in all form fields correctly and try again.</p>
</div>
</div>
</section>

<?php include("../footer.php"); ?>

</body>
</html>
