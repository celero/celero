//celero module -> reduces global variable conflicts
var celero = function() {
    var aside_height;
    var $aside = $('aside');

    $(document).ready(function () {
        /* MAKE SIDEBAR SCROLLABLE ON SMALL BROWSER SIZES */
        aside_height = $aside.height() + 20;
        checkWindowSize();

        /* SHOW DETAILS ON CLICK */
        var phone_number = 1234; // hard-coded number - last 4 digits
        var email_address = "@gmail.com";

        $('#show-phone').click(function () {
            $(this).hide();

            $('#phone_number').text(function (_, txt) {
                return txt.replace('****', phone_number);
            });
        });

        $('#show-email').click(function () {
            $(this).hide();

            $('#email_address').text(function (_, txt) {
                return txt.replace('****', email_address);
            });

            $('#email_address').html('<a href="mailto:' + $('#email_address').text() + '">' + $('#email_address').text() + '</a>');
        });

        /* COLORBOX SETUP */
        $('.profile-image').colorbox();
        $(".show-contact").colorbox({inline:true});
    });

    $(window).resize(checkWindowSize);

    function checkWindowSize() {
        if ($(window).height() < aside_height) {
            $aside.removeClass('fixed');
        } else {
            $aside.addClass('fixed');
        }
    }
}()



