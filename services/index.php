<?php
$page = "services";
include("../header.php");
?>

<section id="feature">
    <div class="container">
        <div class="feature-content">
            <h2>The bricks and mortar</h2>

            <p>The building blocks for the success of your project. Our services include: Website Development, Website
                Redesign, Search Engine Optimisation and Marketing.</p>

            <p class="last">And that's just the start…</p>
        </div>
    </div>
</section>

<section id="services">
    <div class="container" itemscope itemtype="http://www.schema.org/WebPage">
        <div class="section-subtitle">
            <h2>We offer a range of services to suit your needs</h2>
        </div>
        <div class="module" itemscope itemtype="http://www.schema.org/Product">
            <a class="learn-more development" data-learn="#development"></a>
            <h4 itemprop="name">Website Development</h4>

            <div class="description">
                <p itemprop="description">We provide complete website design and implementation using state
                    of the art technologies to create modern, easy to use websites.</p>
            </div>

            <button class="learn-more" data-learn="#development">Learn More</button>
        </div>
        <div class="module" itemscope itemtype="http://www.schema.org/Product">
            <a class="learn-more redesign" data-learn="#redesign"></a>
            <h4 itemprop="name">Website Redesign</h4>

            <div class="description">
                <p itemprop="description">Have an existing website, but want to improve its performance or
                    design? Celero provide solutions for improving existing websites.</p>
            </div>

            <button class="learn-more" data-learn="#redesign">Learn More</button>
        </div>
        <div class="module" itemscope itemtype="http://www.schema.org/Product">
            <a class="learn-more seo" data-learn="#seo"></a>
            <h4 itemprop="name">Search Engine Optimisation</h4>

            <div class="description">
                <p itemprop="description">Increase traffic to your website by utilising the power of search
                    engine optimisations to improve your website's online presence.</p>
            </div>
            <button class="learn-more" data-learn="#seo">Learn More</button>
        </div>
        <div class="module" itemscope itemtype="http://www.schema.org/Product">
            <a class="learn-more marketing" data-learn="#marketing"></a>
            <h4 itemprop="name">Marketing</h4>

            <div class="description">
                <p itemprop="description">Celero is a one stop shop for all for your marketing needs. We offer a
                    customised service to enhance your website marketing opportunities.</p>
            </div>
            <button class="learn-more" data-learn="#marketing">Learn More</button>
        </div>
        <div id="development" class="more-info">
            <h4>Website Development</h4>

            <p class="first">Celero provide professional web development at affordable prices. We utilise
                the latest web development technologies to give your website the competitive edge. We design dynamic
                user interfaces to make your website user friendly to effectively connect to your target market.</p>

            <p>The number of devices, platforms, and browsers that need to work with your site grows daily, as does
                the
                number of users browsing the web with mobile devices. Celero specialise in creating responsive
                websites
                that adapt their layout to the viewing environment, ensuring that your website will look good on
                desktop
                computers, tablets, phones and everything in between.</p>

            <p>This website is responsive! Open the website on your phone and/or tablet and notice how easy it is
                to read and use! If you don't have a phone or tablet, you can just resize your internet browser
                window to see how the layout adapts to different screen sizes.</p>

            <p>We can design a site that is maintained by us or a site that is totally controlled by you. We work in
                close consultation with your business or organisation to create a website that meets your needs and
                exceeds your expectations. Celero make the process of creating a website and going online easy for
                you!</p>

            <p>We will meet you in your workplace to discuss your website requirements. We want you to be absolutely
                happy with what we are offering before you make any commitment to us.
                <a href="../contact/">Contact us</a> for a free, no pressure and no obligation consultation.</p>

        </div>
        <div id="redesign" class="more-info">
            <h4>Website Redesign</h4>

            <p class="first">Celero can upgrade your existing website to take advantage of the latest technological
                developments. We can either refresh your existing website or create a totally new design using
                existing
                information.</p>

            <p>A website redesign by Celero incorporates the latest in web technologies to improve the performance
                and
                image of your existing website and give you a competitive edge. We will analyse your current website
                and
                discuss what improvements can be made with a website redesign by Celero.</p>

            <p><a href="../contact/">Contact us</a> for a free, no pressure and no obligation
                consultation.</p>
        </div>
        <div id="seo" class="more-info">
            <h4>Search Engine Optimisation</h4>

            <p class="first">No matter how great your website looks, how fast it is, or how well it works, it is
                only an
                effective business tool if people can easily find you online. Search engine optimisation is the key
                to
                succeeding in a world where people search online to find what they are looking for and make
                comparisons
                between suppliers and products.</p>

            <p>Search engine optimisation ensures that your website will be accessible to search engines such as
                Google,
                Yahoo and Bing, with well-organised, high-quality content that matches the demands of your target
                market.</p>

            <p><a href="../contact/">Contact us</a> to discuss how you can take advantage of these
                optimisations on your website.</p>
        </div>
        <div id="marketing" class="more-info">
            <h4>Marketing</h4>

            <p class="first">A website user decides within seven seconds whether or not to continue browsing your site.
                With 85% of people visiting a website before deciding to purchase a product or service, you need a
                dynamic, user friendly website that delivers excellent results.</p>

            <p>We will partner with you to develop customised marketing strategies that meet your specific needs. We
                will evaluate your online presence (if any) and recommend ways to maximize your website's
                effectiveness.</p>

            <p><a href="../contact/">Contact us</a> for a no-obligation, free consultation.</p>
        </div>
    </div>
</section>

<?php include("../footer.php"); ?>

</body>
</html>
