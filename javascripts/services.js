$(document).ready(function () {
    var hash = window.location.hash;

    var $active_button = $('#services').find("button[data-learn='" + hash + "']");
    $active_button.addClass('active');

    $(hash).addClass('shown').show();
});

