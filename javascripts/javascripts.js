$(document).ready(function () {
    createSelectMenu();

    var quote_interval = setInterval("changeQuote()", 20000);

    //SMOOTHSCROLL SETUP
    $('.smoothScroll').smoothScroll({speed: 800});
    $('nav select option').smoothScroll({speed: 800});

    //GET WINDOW SIZE
    var smallScreen = checkWindowSize();

    $('#services button.learn-more').click(function () {
        moreInfo($(this), smallScreen);
    });

    $('a.learn-more').click(function () {
        moreInfo($(this), smallScreen);
    });

    $('a.role').click(function () {
        moreInfo($(this), smallScreen);
    });

    $('#testimonials div.person').click(function () {
        showQuote($(this));
        clearInterval(quote_interval);
    });

    $('#portfolio .module, #home-portfolio .module').hover(function () {
        $(this).find('span.rollover').toggleClass('active');
    });

    $('a.-brief_link').hover(function () {
        $(this).closest('.get-started').siblings('.image').find('span').toggleClass('hover');
    });

    $('.section').mouseenter(function () {
        $(this).find('.helper-content').addClass('active');
    });

    $('.section').mouseleave(function () {
        var $input;

        if ($(this).find('input').length > 0)
        {
            $input = $(this).find('input');
        }
        else
        {
            $input = $(this).find('textarea');
        }

        if (!($input.is(":focus")) || $input.is(':checkbox') || $input.is(':radio')){
            $(this).find('.helper-content').removeClass('active');
        }
    });

    $('.section input, .section textarea').focus(function () {
        if (!($(this).is(':checkbox'))) {
            $(this).closest('.section').find('.helper-content').addClass('active');
        }
    });

    $('.section input, .section textarea').focusout(function () {
        $(this).closest('.section').find('.helper-content').removeClass('active');
    });

    $("#contact-form").validate({
        errorPlacement: function (error, element) {
            if (element.is("input:radio")) {
                element.parents("p:first").after(error);
            } else {
                element.after(error);
            }
        },
        submitHandler: function(form) {
            $.ajax({
                type: $(form).attr('method'),
                url: $(form).attr('action'),
                data: $(form).serialize(),
                timeout: 3000,
                success: function(data) {
                    var response = $.trim(data);

                    if (response === 'Success') {
                        $("#contact-form")[0].reset();
                        $('#message-error').hide();
                        $('#message-success').fadeIn();

                        $.smoothScroll({
                            scrollTarget: '#message-success'
                        });
                    }
                    else {
                        $('#message-success').hide();
                        $('#message-error').fadeIn()
                        $.smoothScroll({
                            scrollTarget: '#message-error'
                        });
                    }
                },
                error: function() {alert('Failed to submit form');}
            });
            return false;
        }
    });

    $("#brief-form").validate({
        rules: {
            "project_type[]": {
                required: true,
                minlength: 1
            }
        },
        messages: {
            "project_type[]": "Please select at least one option."
        },
        errorPlacement: function (error, element) {
            if (element.is("input:radio") || element.is("input:checkbox")) {
                element.parents("p:first").after(error);
            } else {
                element.after(error);
            }
        },
        submitHandler: function(form) {
            $.ajax({
                type: $(form).attr('method'),
                url: $(form).attr('action'),
                data: $(form).serialize(),
                timeout: 3000,
                success: function(data) {
                    var response = $.trim(data);

                    if (response === 'Success') {
                        $("#brief-form")[0].reset();
                        $('#message-error').hide();
                        $('#message-success').fadeIn();

                        $.smoothScroll({
                            scrollTarget: '#message-success'
                        });
                    }
                    else {
                        $('#message-success').hide();
                        $('#message-error').fadeIn()
                        $.smoothScroll({
                            scrollTarget: '#message-error'
                        });
                    }
                },
                error: function() {alert('Failed to submit form');}
            });
            return false;
        }
    });

    initPhotoSwipeFromDOM('.image-gallery');
});


//BUILD SELECT MENU
function createSelectMenu() {
    //Build dropdown
    $("<select />").appendTo("nav#select");

    //Create default option "Go to..."
    $("<option />", {
        "selected": "selected",
        "value": "",
        "text": "Go to..."
    }).appendTo("nav#select select");

    //Populate dropdowns with the first menu items
    $("nav#primary ul li a").each(function () {
        var el = $(this);
        $("<option />", {
            "value": el.attr("href"),
            "text": el.text()
        }).appendTo("nav select");
    });

    $("nav#select select").change(function() {
        window.location = $(this).find("option:selected").val();
    });
}

function checkWindowSize() {
    var winW = 630, winH = 460;

    if (document.body && document.body.offsetWidth) {
        winW = document.body.offsetWidth;
        winH = document.body.offsetHeight;
    }

    if (document.compatMode == 'CSS1Compat' && document.documentElement && document.documentElement.offsetWidth) {
        winW = document.documentElement.offsetWidth;
        winH = document.documentElement.offsetHeight;
    }

    if (window.innerWidth && window.innerHeight) {
        winW = window.innerWidth;
        winH = window.innerHeight;
    }

    if (winW < 768 || winH < 400) {
        return true;
    }
    else {
        return false;
    }
}

function moreInfo(element_clicked, screen_is_small) {
    var $this = element_clicked;
    var $more = $this.attr('data-learn');
    var scroll = false;
    var offset = -63;

    if (screen_is_small || $($this).hasClass('role')) {
        scroll = true;
    }

    if ($($this).hasClass('role') && !screen_is_small) {
        offset = -90;
    }

    if ($($more).hasClass('shown')) {
        scrollIfNeeded($more, scroll, offset);

        return;
    }

    $('#services .active').removeClass('active');

    var $active_button = $('#services').find("button[data-learn='" + $more + "']");
    $active_button.addClass('active');

    $('#services .shown').removeClass('shown').hide().promise().done(function () {
        $($more).addClass('shown').fadeIn(300);

        scrollIfNeeded($more, scroll, offset);
    });
}

function showQuote(element_clicked) {
    var $this = element_clicked;
    var $quote_id = $this.attr('data-quote');

    if ($($quote_id).hasClass('shown')) {
        return;
    }

    $('#testimonials li.active').removeClass('active');
    $this.parent().addClass('active');

    $('#testimonials p.shown').removeClass('shown').hide().promise().done(function () {
        $('#testimonials p' + $quote_id).addClass('shown').fadeIn(300).css('display', 'inline');
    });
}


function changeQuote() {
    var $current = $('#testimonials li.active .person');
    var $current_id = $current.attr('data-quote');
    var $next_id;

    if ($current_id === '#first') {
        $next_id = '#second';
    }
    else if ($current_id === '#second') {
        $next_id = '#third'
    }
    else if ($current_id === '#third') {
        $next_id = '#first'
    }

    var $next = $('#testimonials').find("div[data-quote='" + $next_id + "']");

    $('#testimonials li.active').removeClass('active');
    $($next).parent().addClass('active');

    $('#testimonials p.shown').removeClass('shown').hide().promise().done(function () {
        $('#testimonials p' + $next_id).addClass('shown').fadeIn(300).css('display', 'inline');
    });
}


function scrollIfNeeded(scroll_position, scroll, offset) {
    if (scroll) {
        $.smoothScroll({
            scrollTarget: scroll_position,
            offset: offset
        });
    }
}

function initPhotoSwipeFromDOM(gallerySelector) {

    // parse slide data (url, title, size ...) from DOM elements
    // (children of gallerySelector)
    var parseThumbnailElements = function(el) {
        var thumbElements = el.childNodes,
            numNodes = thumbElements.length,
            items = [],
            figureEl,
            linkEl,
            size,
            item;

        for(var i = 0; i < numNodes; i++) {

            figureEl = thumbElements[i]; // <figure> element

            // include only element nodes
            if(figureEl.nodeType !== 1) {
                continue;
            }

            linkEl = figureEl.children[0]; // <a> element

            size = linkEl.getAttribute('data-size').split('x');

            // create slide object
            item = {
                src: linkEl.getAttribute('href'),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10)
            };



            if(figureEl.children.length > 1) {
                // <figcaption> content
                item.title = figureEl.children[1].innerHTML;
            }

            /*
            if(linkEl.children.length > 0) {
                // <img> thumbnail element, retrieving thumbnail url
                item.msrc = linkEl.children[0].getAttribute('src');
            }
            */

            item.el = figureEl; // save link to element for getThumbBoundsFn
            items.push(item);
        }

        return items;
    };

    // find nearest parent element
    var closest = function closest(el, fn) {
        return el && ( fn(el) ? el : closest(el.parentNode, fn) );
    };

    // triggers when user clicks on thumbnail
    var onThumbnailsClick = function(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = closest(eTarget, function(el) {
            return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
        });

        if(!clickedListItem) {
            return;
        }

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = clickedListItem.parentNode,
            childNodes = clickedListItem.parentNode.childNodes,
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if(childNodes[i].nodeType !== 1) {
                continue;
            }

            if(childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }



        if(index >= 0) {
            // open PhotoSwipe if valid index found
            openPhotoSwipe( index, clickedGallery );
        }
        return false;
    };

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    var photoswipeParseHash = function() {
        var hash = window.location.hash.substring(1),
            params = {};

        if(hash.length < 5) {
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if(!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');
            if(pair.length < 2) {
                continue;
            }
            params[pair[0]] = pair[1];
        }

        if(params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        items = parseThumbnailElements(galleryElement);

        // define options (if needed)
        options = {

            // define gallery index (for URL)
            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            /*
            getThumbBoundsFn: function(index) {
                // See Options -> getThumbBoundsFn section of documentation for more info
                var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect();

                return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
            }*/

        };

        // PhotoSwipe opened from URL
        if(fromURL) {
            if(options.galleryPIDs) {
                // parse real index when custom PIDs are used
                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                for(var j = 0; j < items.length; j++) {
                    if(items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                // in URL indexes start from 1
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if( isNaN(options.index) ) {
            return;
        }

        if(disableAnimation) {
            options.showAnimationDuration = 0;
        }

        options.showHideOpacity = true;
        options.hideAnimationDuration = 0;

        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };

    // loop through all gallery elements and bind events
    var galleryElements = document.querySelectorAll( gallerySelector );

    for(var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i+1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    var hashData = photoswipeParseHash();
    if(hashData.pid && hashData.gid) {
        openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
    }
}