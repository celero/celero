//celero module -> reduces global variable conflicts
var celero = function() {
    var aside_height;
    var $aside = $('aside');

    $(document).ready(function () {
        /* SHOW DETAILS ON CLICK */
        var phone_number = 257; // hard-coded number - last 4 digits
        var phone_number_alt = 648;
        var home_phone = 2598;
        var email_address = "mkoncz@gmail.com";

        $('#show-phone').click(function () {
            $(this).hide();

            $('#phone_number').text(function (_, txt) {
                return txt.replace('***', phone_number);
            });
        });

        $('#show-phone-alt').click(function () {
            $(this).hide();

            $('#phone_number_alt').text(function (_, txt) {
                return txt.replace('***', phone_number_alt);
            });
        });

        $('#show-home-phone').click(function () {
            $(this).hide();

            $('#home_phone').text(function (_, txt) {
                return txt.replace('****', home_phone);
            });
        });

        $('#show-email').click(function () {
            $(this).hide();

            $('#email_address').text(function (_, txt) {
                return txt.replace('****', email_address);
            });

            $('#email_address').html('<a href="mailto:' + $('#email_address').text() + '">' + $('#email_address').text() + '</a>');
        });

        /* COLORBOX SETUP */
        $('.profile-image').colorbox();
        $(".show-contact").colorbox({inline:true});
    });

    $(window).load(function () {
        /* MAKE SIDEBAR SCROLLABLE ON SMALL BROWSER SIZES */
        aside_height = $aside.height() + 20;
        checkWindowSize();
    });

    $(window).resize(checkWindowSize);

    function checkWindowSize() {
        if ($(window).height() < aside_height) {
            $aside.removeClass('fixed');
        } else {
            $aside.addClass('fixed');
        }
    }
}()



