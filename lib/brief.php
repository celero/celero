<?php
/**
 * User: Clint Ryan
 * Date: 13/02/13
 * Time: 2:24 PM
 */

/* Name of the user */
$name = $_POST['name'];

/* Email of the user */
$email = $_POST['email'];

/* Contact number of the user */
$number = $_POST['number'];

/* Name of the user's business */
$business = $_POST['business'];

/* URL of the business */
$businessURL = $_POST['url'];

/* Kind of Project => Development, Re-design, Marketing or SEO */
//$projectType = $_POST['project_type'];
$projectType = implode(' ', $_POST['project_type']);

/* Description of project */
$description = $_POST['description'];

/* Budget */
$budget = $_POST['budget'];

/* Deadline of the product */
$deadline = $_POST['deadline'];

/* Next step in the procedure */
$nextStep = $_POST['next_step'];

/* How did they hear about us */
$marketFrom = $_POST['hear'];

/* Additional information */
$additionalInfo = $_POST['additional_info'];

/* User's IP Address */
$ipAddress = $_SERVER['REMOTE_ADDR'];

$required = array($name, $email, $projectType, $description, $budget, $nextStep, $deadline);


if (validateForm($required, $projectType) && check_email_address($email)) {
    $msg = "Name: $name\n";
    $msg .= "Email: $email\n";
    if ($number != "")
        $msg .= "Number: $number\n";
    $msg .= "Business Name: $business\n";
    if ($businessURL != "" && $businessURL != "http://")
        $msg .= "Business URL: $businessURL\n";
    $msg .= "Project Type: $projectType\n";
    $msg .= "Description: $description\n";
    $msg .= "Budget: $budget\n";
    $msg .= "Deadline: $deadline\n";
    $msg .= "Next Step: $nextStep\n";
    if ($marketFrom != "")
        $msg .= "Heard From: $marketFrom\n";
    if ($additionalInfo != "")
        $msg .= "Additional Information: $additionalInfo\n";
    $msg .= "IP Address: $ipAddress\n\n";

    $recipient = "contact@celero.com.au";
    $subject = "Contact Brief - $name";
    $mailHeaders = "From:$email";
    $success = mail($recipient, $subject, $msg, $mailHeaders);

    if ($success) {
        echo "Success";
    } else {
        echo "Error";
    }
} else {
    echo "Invalid Input, Please Try Again. Enable Javascript to determine what fields to fix";
}


function validateForm($required, $projectType)
{
    foreach ($required as &$field) {
        if ($field == "")
            return false;
    }
    /*foreach ($projectType as &$project)
    {
        if ($project == "")
            return false;
    }*/
    return true;
}

function check_email_address($email)
{
    // First, we check that there's one @ symbol, and that the lengths are right
    if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)) {
        // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
        return false;
    }
    //Split it into sections to make life easier
    $email_array = explode("@", $email);
    $local_array = explode(".", $email_array[0]);
    for ($i = 0; $i < sizeof($local_array); $i++) {
        if (!preg_match("/^(([A-Za-z0-9!#$%&'*+\/=?^_`{|}~-][A-Za-z0-9!#$%&'*+\/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/", $local_array[$i])) {
            return false;
        }
    }
    if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
        $domain_array = explode(".", $email_array[1]);
        if (sizeof($domain_array) < 2) {
            return false; // Not enough parts to domain
        }
        for ($i = 0; $i < sizeof($domain_array); $i++) {
            if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])) {
                return false;
            }
        }
    }
    return true;
}
