<?php
$page = "about";
include("../header.php");
?>
<div itemscope itemtype="http://www.schema.org/WebPage">
    <section id="feature">
        <div class="container">
            <div class="feature-content">
                <h2>We're user friendly</h2>

                <p itemprop="description">A love for the web, great design and the latest technologies means your
                    project is more than just a job. Whatever the project size or challenge, we tackle it with the same
                    enthusiasm and passion to keep our clients coming back year after year.</p>
            </div>
        </div>
    </section>

    <section id="about">
        <div class="container">
            <div class="about-container" itemscope itemtype="http://www.schema.org/Person">
                <div class="about-title">
                    <img src="../images/design.png" alt="Design" width="192" height="192">

                    <h3 itemprop="name">Jordan Koncz</h3>

                    <p itemprop="jobTitle">Co-founder,<br/>
                        Head of Front-End Development</p>
                </div>
                <div class="about-description" itemprop="description">
                    <p>Jordan is a perfectionist and has a passion for creating design excellence. He has been
                        developing websites since 2006 and studied Software Engineering at Newcastle University.</p>

                    <p>Jordan believes in making websites that focus on the highest priority goals a client wishes to
                        achieve with their website, and creating simple and elegant design solutions to reach those
                        goals. He also believes in involving the client at all phases of the website development process
                        to ensure they get the highest quality final result - no technical jargon knowledge required,
                        just advice on how to do what you do best - run your business!</p>

                    <p class="last">Jordan has experience with the leading web development frameworks and tools, and
                        will ensure your website is cutting edge and delivered to the highest professional quality.</p>
                </div>
            </div>
            <div class="about-container" itemscope itemtype="http://www.schema.org/Person">
                <div class="about-title">
                    <img src="../images/development.png" alt="Development" width="192" height="192">

                    <h3 itemprop="name">Clinton Ryan</h3>

                    <p itemprop="jobTitle">Co-founder,<br/>Head of Systems Development</p>
                </div>
                <div class="about-description" itemprop="description">
                    <p>Clint has been building fast, beautiful and efficient software systems
                        since 2006. Clint worked as the head of IT at the largest telecommunications training
                        organisation in Australia, and studied Software Engineering at Newcastle University.</p>

                    <p>Clint believes that software should work for the user, not the other way around! As a result, software should be designed to
                        reflect the needs of the client.</p>

                    <p class="last">If there is a faster, more efficient way of solving a business
                        problem - you'll be sure Clint wants you to know about it!</p>
                </div>
            </div>
            <div class="about-container last" itemscope itemtype="http://www.schema.org/Person">
                <div class="about-title">
                    <img src="../images/marketing.png" alt="Marketing" width="192" height="192">

                    <h3 itemprop="name">Bronwyn Reid</h3>

                    <p itemprop="jobTitle">Marketing Consultant</p>
                </div>
                <div class="about-description" itemprop="description">
                    <p>Bronwyn has over 20 years of experience in Public Relations and Marketing, including extensive
                        experience in the Private and Public Health Sector.</p>

                    <p>Bronwyn was the Director for Public Relations and Marketing for The Central Coast and The Hunter
                        Area Health Services for over 10 years, and was the winner of The FIA Direct Marketing National
                        Award of Excellence for the Central Coast Area Health Service Children's Ward Appeal. She has
                        co-ordinated a wide variety of International, National and Regional projects and implemented
                        several award winning campaigns - including campaigns for the NSW and Federal Government.</p>

                    <p class="last">Bronwyn loves working with clients to help refine the marketing, editorial and
                        overall message that their website will send to their respective target audience in order to get
                        the best results!</p>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("../footer.php"); ?>

</body>
</html>
