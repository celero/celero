<?php
$page = "contact";
include("../header.php");
?>
<div itemscope itemtype="http://www.schema.org/ContactPage">
    <section id="feature" xmlns="http://www.w3.org/1999/html">
        <div class="container">
            <div class="feature-content">
                <h2>We're all ears</h2>

                <p>Eager start-ups, established corporations and everything in between, no matter who you are or how big
                    your project is we'd love to discuss how we can help.</p>

                <p class="last">We can't wait to hear from you.</p>
            </div>
        </div>
    </section>

    <section id="contact" itemprop="mainContentOfPage">
        <div class="container" itemscope itemtype="http://www.schema.org/Organization">
            <div class="section-description">
                <p itemprop="contactPoint">You can email us at <a href="mailto:contact@celero.com.au" itemprop="email">contact@celero.com.au</a>
                    or use the contact form below.</p>

                <p class="last">That said, if you have a project in mind, our <a class="planner_link"
                                                                                 title="Celero Project Planner" itemprop="url" itemscope="Project Brief" itemtype="http://www.schema.org/WebPage"
                                                                                 href="../brief/">project
                        brief</a> might be more suitable. It won't take more than five minutes and the more information
                    you can provide us with, the better equipped we will be to respond to your enquiry.</p>
            </div>
            <div class="module">
                <form id="contact-form" action="../lib/mailer.php" method="POST" itemprop="contactPoint">
                    <fieldset id="left">
                        <p class="first">
                            <label for="name">Name</label>
                            <input id="name" type="text" name="name" class="required" minlength="2">
                        </p>

                        <p>
                            <label for="email">Email</label>
                            <input id="email" type="email" name="email" novalidate="novalidate" class="required email">
                        </p>

                        <p>
                            <label for="number">Telephone</label>
                            <input id="number" type="tel" name="number">
                        </p>

                        <p>
                            <label>Preferred method of contact</label><br>
                            <input id="contact_1" class="first required" type="radio" value="Email"
                                   name="preferred_contact">
                            <label for="contact_1">Email</label>
                            <input id="contact_2" type="radio" value="Telephone" name="preferred_contact">
                            <label for="contact_2">Telephone</label>
                        </p>
                    </fieldset>
                    <fieldset id="right">
                        <label for="message">Message</label>
                        <textarea id="message" name="message" class="required"></textarea>
                        <input class="button" type="submit" value="Send Message">
                    </fieldset>
                </form>
            </div>
            <div id="message-success" class="message-response">
                <p>Your message was successfully sent. Thank you for contacting us.</p>
            </div>
            <div id="message-error" class="message-response">
                <p>Sorry, something went wrong. Please fill in all form fields correctly and try again.</p>
            </div>
        </div>
    </section>
</div>
<?php include("../footer.php"); ?>

</body>
</html>
